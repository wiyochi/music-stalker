mod api;
mod database;
mod handlers;
mod models;

extern crate pretty_env_logger;
#[macro_use]
extern crate log;

#[tokio::main]
#[cfg(not(tarpaulin_include))]
async fn main() {
    pretty_env_logger::init();

    let db = database::init_db();
    let routes = api::routes(db);

    warp::serve(routes).run(([127, 0, 0, 1], 8080)).await;
}

#[cfg(test)]
mod tests {
    use crate::{api, database, models};
    use serde::Serialize;
    use warp::test::request;

    #[derive(Serialize)]
    struct CorruptedUser {
        id: String,
    }

    fn user() -> models::User {
        models::User::new("user-id", "user-name")
    }

    #[tokio::test]
    async fn test_add_user() {
        let db = database::init_db();
        let routes = api::routes(db.clone());

        let response = request()
            .method("POST")
            .path("/addUser")
            .json(&user())
            .reply(&routes)
            .await;

        assert_eq!(response.status(), warp::http::StatusCode::CREATED);
        match db.lock() {
            Ok(users) => assert_eq!(users[0], user()),
            Err(_) => panic!("lock failed"),
        };
    }

    #[tokio::test]
    async fn test_add_corrupted_user() {
        let db = database::init_db();
        let routes = api::routes(db.clone());

        let response = request()
            .method("POST")
            .path("/addUser")
            .json(&CorruptedUser {
                id: "user-id".to_string(),
            })
            .reply(&routes)
            .await;

        assert_eq!(response.status(), warp::http::StatusCode::BAD_REQUEST);
    }

    #[tokio::test]
    async fn test_get_users() {
        let db = database::init_db();
        let routes = api::routes(db.clone());

        match db.lock() {
            Ok(mut users) => users.push(user()),
            Err(_) => panic!("lock failed"),
        };

        let response = request()
            .method("GET")
            .path("/getUsers")
            .reply(&routes)
            .await;

        assert_eq!(response.status(), warp::http::StatusCode::OK);
        let users_result = serde_json::from_slice::<Vec<models::User>>(response.body());
        assert_eq!(users_result.ok(), Some(vec![user()]));
    }
}
