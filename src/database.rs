use crate::models;
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub type Database = Arc<Mutex<Vec<models::User>>>;

pub fn init_db() -> Database {
    Arc::new(Mutex::new(Vec::new()))
}

#[derive(Error, Debug, Serialize, Deserialize)]
pub enum Error {
    #[error("Mutex lock error")]
    LockError,
}

pub fn add_user(db: Database, user: models::User) -> Result<(), Error> {
    debug!("Adding user : {:#?}", user);
    match db.lock() {
        Ok(mut users) => {
            users.push(user);
            Ok(())
        }
        Err(_) => Err(Error::LockError),
    }
}

pub fn get_users(db: Database) -> Result<Vec<models::User>, Error> {
    debug!("Getting users");
    match db.lock() {
        Ok(users) => Ok(users.clone()),
        Err(_) => Err(Error::LockError),
    }
}
