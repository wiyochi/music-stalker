use crate::{database, models};
use std::convert::Infallible;
use warp::Reply;

pub async fn create_user(
    db: database::Database,
    new_user: models::User,
) -> Result<impl Reply, Infallible> {
    match database::add_user(db, new_user) {
        Ok(_) => Ok(warp::http::StatusCode::CREATED),
        Err(_) => Ok(warp::http::StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_users(db: database::Database) -> Result<impl Reply, Infallible> {
    match database::get_users(db) {
        Ok(users) => Ok(warp::reply::with_status(
            warp::reply::json(&users),
            warp::http::StatusCode::OK,
        )),
        Err(error) => Ok(warp::reply::with_status(
            warp::reply::json(&error),
            warp::http::StatusCode::INTERNAL_SERVER_ERROR,
        )),
    }
}
