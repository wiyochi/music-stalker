use std::convert::Infallible;

use crate::{database::Database, handlers};
use warp::{self, Filter};

fn with_db(db: Database) -> impl Filter<Extract = (Database,), Error = Infallible> + Clone {
    warp::any().map(move || db.clone())
}

pub fn routes(
    db: Database,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    add_user(db.clone()).or(get_users(db))
}

pub fn add_user(
    db: Database,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::path!("addUser")
        .and(warp::post())
        .and(with_db(db))
        .and(warp::body::json())
        .and_then(handlers::create_user)
}

pub fn get_users(
    db: Database,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::path!("getUsers")
        .and(warp::get())
        .and(with_db(db))
        .and_then(handlers::get_users)
}
