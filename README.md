# Music Stalker

[![pipeline status](https://gitlab.com/wiyochi/music-stalker/badges/master/pipeline.svg)](https://gitlab.com/wiyochi/music-stalker/-/commits/master)
[![coverage report](https://gitlab.com/wiyochi/music-stalker/badges/master/coverage.svg)](https://gitlab.com/wiyochi/music-stalker/-/commits/master)

A simple web app to stalk your friends music, listen with them and share your favourites

## Run
### Backend
Clone this repo and run :
```sh
cargo run --release
```
You can run it with logs (trace / debug / info / warn / error) :
```sh
RUST_LOG=trace cargo run --release
```

## Authors
 - wiyochi (Mathieu Arquillière)
